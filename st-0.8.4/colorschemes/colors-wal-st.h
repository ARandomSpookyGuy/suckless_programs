const char *colorname[] = {

  [0] = "#0D0D0D", /* black   */
  [1] = "#262626", /* red     */
  [2] = "#D27925", /* green   */
  [3] = "#4d4d4d", /* yellow  */
  [4] = "#D27925", /* blue    */
  [5] = "#333333", /* magenta */
  [6] = "#D27925", /* cyan    */
  [7] = "#D27925", /* white   */

  /* 8 bright colors */
  [8]  = "#ffffff",  /* black   */
  [9]  = "#D27925",  /* red     */
  [10] = "#333333", /* green   */
  [11] = "#D27925", /* yellow  */
  [12] = "#4d4d4d", /* blue    */
  [13] = "#D27925", /* magenta */
  [14] = "#262626", /* cyan    */
  [15] = "#D27925", /* white   */

  /* special colors */
  [256] = "#16183d", /* background */
  [257] = "#cfdbe1", /* foreground */
  [258] = "#cfdbe1",     /* cursor */



  /* 8 normal colors */
  //[0] = "#0D0D0D", [> black   <]
  //[1] = "#262626", [> red     <]
  //[2] = "#ff3333", [> green   <]
  //[3] = "#4d4d4d", [> yellow  <]
  //[4] = "#ff3333", [> blue    <]
  //[5] = "#333333", [> magenta <]
  //[6] = "#ff3333", [> cyan    <]
  //[7] = "#D27925", [> white   <]

  //[> 8 bright colors <]
  //[8]  = "#ffffff",  [> black   <]
  //[9]  = "#ff3333",  [> red     <]
  //[10] = "#333333", [> green   <]
  //[11] = "#ff3333", [> yellow  <]
  //[12] = "#4d4d4d", [> blue    <]
  //[13] = "#ff3333", [> magenta <]
  //[14] = "#262626", [> cyan    <]
  //[15] = "#D27925", [> white   <]

  //[> special colors <]
  //[256] = "#16183d", [> background <]
  //[257] = "#cfdbe1", [> foreground <]
  //[258] = "#cfdbe1",     [> cursor <]
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
